import React,{ useContext, useEffect }from 'react';
import Router from 'next/router';
import UserContext from '../../UserContext'

export default function Logout(){
    
        const {unsetUser, setUser} = useContext(UserContext)
        
        useEffect(() => {
            unsetUser();
            Router.push('/login')
        },[])

        
                
        return null
        
}